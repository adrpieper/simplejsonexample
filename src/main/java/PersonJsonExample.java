import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Adi on 2017-05-29.
 */
public class PersonJsonExample {


    public static void main(String[] args) throws JsonProcessingException {


        ObjectMapper mapper = new ObjectMapper();

        System.out.println(mapper.writeValueAsString(new Person("Adi",10)));
    }
}
